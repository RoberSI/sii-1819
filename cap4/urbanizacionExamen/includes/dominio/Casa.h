// Casa.h: interface for the Casa class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CASA_H__99D83157_7B28_4E00_A985_F76100E8D4BC__INCLUDED_)
#define AFX_CASA_H__99D83157_7B28_4E00_A985_F76100E8D4BC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Bloque.h"
#include "Techo.h"

class Casa  
{
	Bloque laBase;
	Techo elTecho;

public:
	Casa(float,float,float);
	virtual ~Casa();
	void setPosicion(float,float,float);
	void dibuja();

};

#endif // !defined(AFX_CASA_H__99D83157_7B28_4E00_A985_F76100E8D4BC__INCLUDED_)
