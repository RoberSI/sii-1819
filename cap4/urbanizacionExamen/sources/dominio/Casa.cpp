// Casa.cpp: implementation of the Casa class.
//
//////////////////////////////////////////////////////////////////////

#include "..\..\INCLUDES\DOMINIO\Casa.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Casa::Casa(float ancho, float altoBase, float altoTejado)
{
	this->laBase.setBase(ancho);
	this->elTecho.setBase(ancho);

	this->laBase.setAltura(altoBase);
	this->elTecho.setAltura(altoTejado);

}

Casa::~Casa()
{

}

void Casa::setPosicion(float ax,float ay,float az)
{
	this->laBase.setPosicion(ax,ay,az);
	this->elTecho.setPosicion(ax,ay+(this->laBase.getAltura()),az);
}

void Casa::dibuja()
{
	this->elTecho.dibuja();
	this->laBase.dibuja();
}


