// Techo.cpp: implementation of the Techo class.
//
//////////////////////////////////////////////////////////////////////

#include "..\..\INCLUDES\DOMINIO\Techo.h"
#include "..\..\INCLUDES\comunes\glut.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Techo::Techo()
{

}

Techo::~Techo()
{

}

void Techo::dibuja()
{
	glColor3ub(255,0,0);
	glTranslatef(x,y,z);
	glRotatef(-90.0f, 1.0f, 0.0f,0.0f);
	glutSolidCone(this->base,this->altura,20,20);
	glRotatef(90.0f, 1.0f, 0.0f,0.0f);
	
	//glutSolidSphere(altura,20,20);
	
	glTranslatef(-x,-y,-z);
}
