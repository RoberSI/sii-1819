INCLUDE_DIRECTORIES("${PROJECT_INCLUDE_DIR}")

SET(COMMON_SRCS 
	Urbanizacion.cpp Casa.cpp Techo.cpp Bloque.cpp)
				
ADD_EXECUTABLE(urbanizacion ../Urbanizacion/principal.cpp ${COMMON_SRCS})

TARGET_LINK_LIBRARIES(urbanizacion glut GL GLU)
