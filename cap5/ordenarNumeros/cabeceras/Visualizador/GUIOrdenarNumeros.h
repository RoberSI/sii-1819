// GUIOrdenarNumeros.h: interface for the GUIOrdenarNumeros class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GUIORDENARNUMEROS_H__2B00AC69_C0AB_428C_8F4C_B668388767C3__INCLUDED_)
#define AFX_GUIORDENARNUMEROS_H__2B00AC69_C0AB_428C_8F4C_B668388767C3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <vector>

class GUIOrdenarNumeros  
{
public:
	GUIOrdenarNumeros();
	virtual ~GUIOrdenarNumeros();
	void visualizarResultados(std::vector<double> &);
	void solicitarNumeros(std::vector<double> &);
};

#endif // !defined(AFX_GUIORDENARNUMEROS_H__2B00AC69_C0AB_428C_8F4C_B668388767C3__INCLUDED_)
