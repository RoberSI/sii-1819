// OrdenarNumeros.cpp: implementation of the OrdenarNumeros class.
//
//////////////////////////////////////////////////////////////////////
#include <algorithm>
#include "./../../cabeceras/OrdenarNumeros/OrdenarNumeros.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

OrdenarNumeros::OrdenarNumeros()
{

}

OrdenarNumeros::~OrdenarNumeros()
{

}

void OrdenarNumeros::introducirDatos()
{
	elVisualizador.solicitarNumeros(elVectorNumeros);
	std::sort(elVectorNumeros.begin(),elVectorNumeros.end());
}

void OrdenarNumeros::visualizarResultados()
{
	this->elVisualizador.visualizarResultados(this->elVectorNumeros);
}


int main()
{
	OrdenarNumeros elOrdenar;

	elOrdenar.introducirDatos();
	elOrdenar.visualizarResultados();

	return 0;
}
