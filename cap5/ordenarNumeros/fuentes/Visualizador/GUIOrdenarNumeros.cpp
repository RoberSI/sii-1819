// GUIOrdenarNumeros.cpp: implementation of the GUIOrdenarNumeros class.
//
//////////////////////////////////////////////////////////////////////

#include "./../../cabeceras/Visualizador/GUIOrdenarNumeros.h"

#include <algorithm>
#include <iostream>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

GUIOrdenarNumeros::GUIOrdenarNumeros()
{

}

GUIOrdenarNumeros::~GUIOrdenarNumeros()
{

}


void GUIOrdenarNumeros::solicitarNumeros(std::vector<double> &elVectorNumeros)
{
	bool introducirDatos = true;
	double valor;

	std::cout<<"Esta aplicacion ordena los valores de forma creciente"<<std::endl;
	std::cout<<"Introducir la lista de numeros y poner cero para salir"<<std::endl;

	while(introducirDatos == true)
	{
		std::cin>>valor;
		if(valor != 0)
			elVectorNumeros.push_back(valor);
		else
			introducirDatos = false;
	}
}

void visualizarDatos(double);

void GUIOrdenarNumeros::visualizarResultados(std::vector<double> &elVectorNumeros)
{
	std::cout<<"Lista ordenada"<<std::endl;
	std::cout<<"=============="<<std::endl;
	std::for_each(elVectorNumeros.begin(),elVectorNumeros.end(),visualizarDatos);
	char caracter;
	std::cout<<"Pulsar cualquier tecla para salir"<<std::endl;
	std::cin>> caracter;
}

void visualizarDatos(double valor)
{
	std::cout<<valor<<std::endl;

}


